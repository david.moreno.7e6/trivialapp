package com.example.trivialapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.example.trivialapp.databinding.ActivityLaunchBinding
import com.example.trivialapp.databinding.ActivityMenuBinding

class LaunchScreen : AppCompatActivity() {

    lateinit var binding: ActivityLaunchBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityLaunchBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val handler= Handler(Looper.getMainLooper())
        handler.postDelayed({
            val toMenu= Intent(this, MenuScreen::class.java)
            startActivity(toMenu)
            finish()
        }, 2000)

    }
}