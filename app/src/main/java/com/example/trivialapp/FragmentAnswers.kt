package com.example.trivialapp
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentResultListener
import com.example.trivialapp.databinding.FragmentAnswersBinding
import kotlin.concurrent.timer

class FragmentAnswers : androidx.fragment.app.Fragment(), View.OnClickListener {

    lateinit var binding: FragmentAnswersBinding
    lateinit var list: ArrayList<String>
    lateinit var timer: CountDownTimer
    lateinit var name: String
    var totalScore= 0
    var score=0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAnswersBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            timer = object : CountDownTimer(11000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    binding.countdown.progress = (millisUntilFinished / 1000).toInt()
                }

                override fun onFinish() {
                    binding.points.visibility = View.VISIBLE
                    binding.plus.visibility = View.VISIBLE
                    binding.points.text = "0"
                    var next = object : CountDownTimer(3000, 1000) {
                        override fun onTick(millisUntilFinished: Long) {
                        }

                        override fun onFinish() {
                            val bundle = Bundle()
                            bundle.putInt("points", 0)
                            bundle.putStringArrayList("list", list)
                            parentFragmentManager.setFragmentResult(
                                "dades",
                                bundle
                            )

                            parentFragmentManager.beginTransaction().apply {
                                replace(R.id.fragmentMenu, ChooseAnswers())
                                setReorderingAllowed(true)
                                addToBackStack("name")
                                commit()
                            }
                        }
                    }.start()
                }
            }.start()
            parentFragmentManager.setFragmentResultListener(
                "datos", this
            ) { requestKey: String, result: Bundle ->
                val tematica = result.getString("tematica")
                list = result.getStringArrayList("list") as ArrayList<String>
                totalScore= result.getInt("result")
                name= result.getString("user").toString()
                binding.tittleAnswer.text = tematica
                val random =(0..3).random()
                val background = binding.answers
                when (tematica) {
                    "HISTORIA" -> {

                        background.setBackgroundColor(Color.parseColor("#FFA500"))
                        val questions = arrayOf(
                            arrayOf(
                                "En que año se produjo la Revolución Francesa?",
                                "1759",
                                "1789",
                                "1745",
                                "2010"
                            ),
                            arrayOf(
                                "¿Qué personaje romano da nombre al mes de julio?",
                                "Julio Iglesias",
                                "Constantino",
                                "Augusto",
                                "Julio César"
                            ),
                            arrayOf(
                                "¿Cuándo empezó la Primera Guerra Mundial?",
                                "1914",
                                "1917",
                                "1910",
                                "1913"
                            ),
                            arrayOf(
                                "¿Cuántos años duró realmente la Guerra de los 100 años?",
                                "116"
                            )
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.revolucion)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.batalla)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FourthAnswer.setOnClickListener(this)
                                }

                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.julio)
                                    binding.FourthAnswer.setOnClickListener {
                                        binding.FourthAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.batalla)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.primeraguerramundial)
                                    binding.FirstAnswer.setOnClickListener {
                                        binding.FirstAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.batalla)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            editText()
                            binding.imageEdit.setImageResource(R.drawable.guerra)
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.batalla)
                                    }
                                    "CIENTO DIECISEIS" -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.batalla)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                    "DEPORTE" -> {
                        background.setBackgroundColor(Color.parseColor("#FF8C00"))
                        val questions = arrayOf(
                            arrayOf(
                                "Quién es el máximo goleador de la historia del fútbol?",
                                "Messi",
                                "Cristiano Ronaldo",
                                "Pelé",
                                "Maradona"
                            ),
                            arrayOf(
                                "Si juegas en la NFL ¿qué deporte practicas?",
                                "Hockey",
                                "Baloncesto",
                                "Béisbol",
                                "Fútbol Americano"
                            ),
                            arrayOf(
                                "¿A qué deporte pertenece el córner corto?",
                                "Fútbol",
                                "Hockey",
                                "Ping Pong",
                                "Fútbol Sala"
                            ),
                            arrayOf(
                                "En tenis, un punto ganado directamente en el servicio sin que el oponente toque con su raqueta la bola, se le denomina...",
                                "ACE"
                            )
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.gol)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.ali)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.nfl)
                                    binding.FourthAnswer.setOnClickListener {
                                        binding.FourthAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.ali)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.deporte)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.ali)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            editText()
                            binding.imageEdit.setImageResource(R.drawable.tennis)
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.ali)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                    "CIENCIA" -> {
                        background.setBackgroundColor(Color.parseColor("#9ACD32"))
                        val questions = arrayOf(
                            arrayOf(
                                "¿Quién inventó la penicilina?",
                                "Einstein",
                                "Fleming",
                                "Tesla",
                                "Edison"
                            ),
                            arrayOf(
                                "¿Cuál es el gas más abundante en la atmósfera de la Tierra?",
                                "Nitrógeno",
                                "Oxígeno",
                                "Helio",
                                "Dióxido de carbono"
                            ),
                            arrayOf(
                                "¿Cuántos planetas componen el sistema Solar?",
                                "7",
                                "9",
                                "8",
                                "12"
                            ),
                            arrayOf("Representación en la tabla periódica del Potasio", "K")
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.penizilina)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.ciencia)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.gases)
                                    binding.FirstAnswer.setOnClickListener {
                                        binding.FirstAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.ciencia)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.sol)
                                    binding.ThirdAnswer.setOnClickListener {
                                        binding.ThirdAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.ciencia)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            binding.imageEdit.setImageResource(R.drawable.potasio)
                            editText()
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.ciencia)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                    "CINE" -> {
                        background.setBackgroundColor(Color.parseColor("#696969"))
                        val questions = arrayOf(
                            arrayOf(
                                "¿En que año se estrenó Titanic?",
                                "2000",
                                "1997",
                                "2003",
                                "1992"
                            ),
                            arrayOf(
                                "¿Qué película es la más taquillera de la história?",
                                "Avatar",
                                "Avengers: End Game",
                                "Transformers 4",
                                "Stars Wars: Episodio 7"
                            ),
                            arrayOf(
                                "¿Cuánto dura la película más larga de la história?",
                                "10 horas",
                                "17 horas",
                                "25 horas",
                                "21 horas"
                            ),
                            arrayOf(
                                "A qué personaje de ficción pertenece esta famosa frase: Yo soy tu padre",
                                "Darth Vader"
                            )
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.titanic)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.palomitas)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.taquillera)
                                    binding.FirstAnswer.setOnClickListener {
                                        binding.FirstAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.palomitas)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.reloj)
                                    binding.FourthAnswer.setOnClickListener {
                                        binding.FourthAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.palomitas)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            binding.imageEdit.setImageResource(R.drawable.padre)
                            editText()
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.palomitas)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                    "MÚSICA" -> {
                        background.setBackgroundColor(Color.parseColor("#7FFFD4"))
                        val questions = arrayOf(
                            arrayOf(
                                "¿Qué grupo de rock clásico era conocido por estar drogado durante sus primeros conciertos?",
                                "The Beatles",
                                "Rolling Stones",
                                "Guns n Roses",
                                "Pearl Jam"
                            ),
                            arrayOf(
                                "¿Cuál es el verdadero nombre del rapero Notorious B.I.G?",
                                "Christopher Wallace",
                                "Andre Young",
                                "Adam Young",
                                "Christian Wallace"
                            ),
                            arrayOf(
                                "¿En que año falleció Michael Jackson?",
                                "2008",
                                "2011",
                                "2009",
                                "2006"
                            ),
                            arrayOf(
                                "Artista más escuchado en 2022 en el género musical K-Pop",
                                "BTS"
                            )
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.rock)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.jackson)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.biggie)
                                    binding.FirstAnswer.setOnClickListener {
                                        binding.FirstAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.jackson)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.michael)
                                    binding.ThirdAnswer.setOnClickListener {
                                        binding.ThirdAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.jackson)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                            }

                        } else {
                            binding.imageEdit.setImageResource(R.drawable.bts)
                            editText()
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.jackson)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                    "GEOGRAFÍA" -> {
                        background.setBackgroundColor(Color.parseColor("#FFDAB9"))
                        val questions = arrayOf(
                            arrayOf(
                                "¿Cúal es la capital de Turquía?",
                                "Istanbul",
                                "Argos",
                                "Zagreb",
                                "Ankara"
                            ),
                            arrayOf(
                                "¿Cuál es el rio más caudaloso del mundo??",
                                "Ebro",
                                "Mississipi",
                                "Amazonas",
                                "Nilo"
                            ),
                            arrayOf(
                                "¿Cuál es la montaña más alta del mundo?",
                                "Machu Picchu",
                                "Teide",
                                "K2",
                                "Everest"
                            ),
                            arrayOf("Hawuái és un país?", "NO")
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.turquia)
                                    binding.FourthAnswer.setOnClickListener {
                                        binding.FourthAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.mundo)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.rio)
                                    binding.ThirdAnswer.setOnClickListener {
                                        binding.ThirdAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.mundo)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.montanya)
                                    binding.FourthAnswer.setOnClickListener {
                                        binding.FourthAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.mundo)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            binding.imageEdit.setImageResource(R.drawable.hawaii)
                            editText()
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.mundo)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                    "ARTE" -> {
                        background.setBackgroundColor(Color.parseColor("#ffffff"))
                        val questions = arrayOf(
                            arrayOf(
                                "¿Quién pintó el Guernica?",
                                "Paloma Picasso",
                                "Víncent Van Gogh",
                                "Pablo Picasso",
                                "Henri Matisse"
                            ),
                            arrayOf(
                                "¿Cuál es el nombre del famosisímo cuadro de Edvard Munch?",
                                "El fantasma",
                                "El sonido",
                                "El grito",
                                "El pánico"
                            ),
                            arrayOf(
                                "¿Cómo se llama la famosa estatua de la imagen?",
                                "El pensador",
                                "Hombre pensante",
                                "Razocinio",
                                "Pensamiento"
                            ),
                            arrayOf("Su apellido es DaVinci, su nombre es...", "LEONARDO")
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.guernica)
                                    binding.ThirdAnswer.setOnClickListener {
                                        binding.ThirdAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.pincel)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.munch)
                                    binding.ThirdAnswer.setOnClickListener {
                                        binding.ThirdAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.pincel)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.estatua)
                                    binding.FirstAnswer.setOnClickListener {
                                        binding.FirstAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.pincel)
                                    }
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            binding.imageEdit.setImageResource(R.drawable.leonardo)
                            editText()
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.pincel)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }


                            }
                        }

                    }
                    "ADIVINANZA" -> {
                        background.setBackgroundColor(Color.parseColor("#AFEEEE"))
                        val questions = arrayOf(
                            arrayOf(
                                "Siempre va por la tierra sin ensuciarse. ¿Qué es?",
                                "Gusano",
                                "Topo",
                                "Sombra",
                                "Calcetín"
                            ),
                            arrayOf(
                                "¿Existe un ser vivo capaz de beber agua con los pies. ¿Cuál es?",
                                "Cigüeña",
                                "Árbol",
                                "Canguro",
                                "Hormiga"
                            ),
                            arrayOf(
                                "Camino sobre las olas y tengo vestidos blancos. Si el viento me empuja fuerte, mucho más rápido ando.",
                                "Ángel",
                                "Dios",
                                "Delfín",
                                "Velero"
                            ),
                            arrayOf(
                                "¿Qué número tiene el mismo número de letras que el valor que expresa?",
                                "CINCO"
                            )
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.tierra)
                                    binding.ThirdAnswer.setOnClickListener {
                                        binding.ThirdAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.interrogante)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.FourthAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.perrobebiendo)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.interrogante)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FourthAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.oceano)
                                    binding.FourthAnswer.setOnClickListener {
                                        binding.FourthAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.interrogante)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            binding.imageEdit.setImageResource(R.drawable.numbers)
                            editText()
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.interrogante)
                                    }
                                    "5" -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.interrogante)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                    "LENGUA" -> {
                        background.setBackgroundColor(Color.parseColor("#B22222"))
                        val questions = arrayOf(
                            arrayOf(
                                "la secuencia de dos vocales distintas que se pronuncian dentro de la misma sílaba, se conoce cómo...",
                                "Hiato",
                                "Leísmo",
                                "Diptongo",
                                "Rima"
                            ),
                            arrayOf(
                                "¿Cuántas letras componen el abecedario?",
                                "27",
                                "25",
                                "31",
                                "22"
                            ),
                            arrayOf(
                                "Despacio és un...",
                                "Adjetivo",
                                "Adverbio",
                                "Sustantivo",
                                "Verbo"
                            ),
                            arrayOf(
                                "Completa la frase con una palabra: Pon la pasta cuando el agua...",
                                "HIERVA"
                            )
                        )
                        binding.question.text = questions[random][0]
                        if (random != 3) {
                            binding.FirstAnswer.text = questions[random][1]
                            binding.SecondAnswer.text = questions[random][2]
                            binding.ThirdAnswer.text = questions[random][3]
                            binding.FourthAnswer.text = questions[random][4]
                            when (random) {
                                0 -> {
                                    binding.imagequestion.setImageResource(R.drawable.article)
                                    binding.ThirdAnswer.setOnClickListener {
                                        binding.ThirdAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.lengua)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.SecondAnswer.setOnClickListener(this)
                                    binding.FourthAnswer.setOnClickListener(this)
                                }
                                1 -> {
                                    binding.imagequestion.setImageResource(R.drawable.abecedario)
                                    binding.FirstAnswer.setOnClickListener {
                                        binding.FirstAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.lengua)
                                    }
                                    binding.SecondAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FourthAnswer.setOnClickListener(this)
                                }
                                2 -> {
                                    binding.imagequestion.setImageResource(R.drawable.despacio)
                                    binding.SecondAnswer.setOnClickListener {
                                        binding.SecondAnswer.setBackgroundColor(Color.parseColor("#AFE1AF"))
                                        correct()
                                        binding.imageresult.setImageResource(R.drawable.lengua)
                                    }
                                    binding.FirstAnswer.setOnClickListener(this)
                                    binding.ThirdAnswer.setOnClickListener(this)
                                    binding.FourthAnswer.setOnClickListener(this)
                                }
                            }
                        } else {
                            binding.imageEdit.setImageResource(R.drawable.pasta)
                            editText()
                            binding.submitEdit.setOnClickListener {
                                when (binding.editQuestion.text.toString().uppercase()) {
                                    questions[random][1] -> {
                                        correctEdit()
                                        binding.imageresult.setImageResource(R.drawable.lengua)
                                    }
                                    else -> {
                                        incorrectEdit()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        override fun onClick(p0: View?) {
            timer.cancel()
            val button = p0 as Button
            binding.points.visibility = View.VISIBLE
            binding.plus.visibility = View.VISIBLE
            binding.points.text = "0"
            totalScore += 0
            button.setBackgroundColor(Color.parseColor("#B22222"))
                var next = object : CountDownTimer(2000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                    }

                    override fun onFinish() {
                        if (list.size != 0){
                            val bundle = Bundle()
                            bundle.putInt("points", totalScore)
                            bundle.putStringArrayList("list", list)
                            parentFragmentManager.setFragmentResult(
                                "dades",
                                bundle
                            )

                            parentFragmentManager.beginTransaction().apply {
                                replace(R.id.fragmentMenu, ChooseAnswers())
                                setReorderingAllowed(true)
                                addToBackStack("name")
                                commit()
                            }
                        }
                        else {
                            parentFragmentManager.setFragmentResult(
                                "total",
                                bundleOf("totalScore" to totalScore)
                            )
                            parentFragmentManager.beginTransaction().apply {
                                replace(R.id.fragmentMenu, ResultFragment())
                                setReorderingAllowed(true)
                                addToBackStack("name")
                                commit()
                            }
                        }
                    }
                }.start()
            }
    fun correct(){
        score = binding.countdown.progress * 10
        totalScore += score
        binding.imageresult.visibility = View.VISIBLE
        binding.points.text = score.toString()
        binding.points.visibility = View.VISIBLE
        binding.plus.visibility = View.VISIBLE
        timer.cancel()
        var next = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                if (list.size != 0){
                    val bundle = Bundle()
                    bundle.putInt("points", totalScore)
                    bundle.putStringArrayList("list", list)
                    parentFragmentManager.setFragmentResult(
                        "dades",
                        bundle
                    )

                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentMenu, ChooseAnswers())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }
                else {
                    parentFragmentManager.setFragmentResult(
                        "total",
                        bundleOf("totalScore" to totalScore)
                    )
                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentMenu, ResultFragment())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }
            }
        }.start()
    }
    fun editText(){
        binding.FirstAnswer.visibility = View.INVISIBLE
        binding.SecondAnswer.visibility = View.INVISIBLE
        binding.ThirdAnswer.visibility = View.INVISIBLE
        binding.FourthAnswer.visibility = View.INVISIBLE
        binding.submitEdit.visibility = View.VISIBLE
        binding.editQuestion.visibility = View.VISIBLE
    }
    fun correctEdit(){
        timer.cancel()
        score = binding.countdown.progress * 15
        binding.imageresult.visibility = View.VISIBLE
        binding.points.text = score.toString()
        binding.points.visibility = View.VISIBLE
        binding.plus.visibility = View.VISIBLE
        totalScore += score
        timer.cancel()
        var next = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                if (list.size != 0){
                    val bundle = Bundle()
                    bundle.putInt("points", totalScore)
                    bundle.putStringArrayList("list", list)
                    parentFragmentManager.setFragmentResult(
                        "dades",
                        bundle
                    )

                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentMenu, ChooseAnswers())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }
                else {
                    parentFragmentManager.setFragmentResult(
                        "total",
                        bundleOf("totalScore" to totalScore)
                    )
                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentMenu, ResultFragment())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }
            }
        }.start()
    }
    fun incorrectEdit(){
        timer.cancel()
        binding.points.visibility = View.VISIBLE
        binding.plus.visibility = View.VISIBLE
        binding.points.text = "0"
        totalScore += 0
        var next = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                if (list.size != 0){
                    val bundle = Bundle()
                    bundle.putInt("points", totalScore)
                    bundle.putStringArrayList("list", list)
                    parentFragmentManager.setFragmentResult(
                        "dades",
                        bundle
                    )

                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentMenu, ChooseAnswers())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }
                else {
                    parentFragmentManager.setFragmentResult(
                        "total",
                        bundleOf("totalScore" to totalScore)
                    )
                    parentFragmentManager.beginTransaction().apply {
                        replace(R.id.fragmentMenu, ResultFragment())
                        setReorderingAllowed(true)
                        addToBackStack("name")
                        commit()
                    }
                }
            }
        }.start()
    }
    }




