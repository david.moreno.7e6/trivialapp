package com.example.trivialapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentResultListener
import com.example.trivialapp.databinding.FragmentResultBinding
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import kotlin.properties.Delegates

class ResultFragment : Fragment() {

    lateinit var binding: FragmentResultBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResultBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        parentFragmentManager.setFragmentResultListener("total", this,
            FragmentResultListener { requestKey: String, result: Bundle ->
                val points = result.getInt("totalScore")
                parentFragmentManager.setFragmentResultListener("jugador", this,
                    FragmentResultListener { requestKey: String, result: Bundle ->
                        val name = result.getString("jugador")!!.uppercase()
                        binding.points.text= "ENHORABUENA $name\n\n\nHAS CONSEGUIDO...\n\n\n$points PUNTOS!"
                        if (fileExists("Score.txt")==false){
                            kotlin.io.path.createTempFile("Score", ".txt")
                        }
                        writeFile(name, points)
                        readFile()
                        binding.menu.setOnClickListener {
                            parentFragmentManager.beginTransaction().apply {
                                replace(R.id.fragmentMenu, MenuFragment())
                                setReorderingAllowed(true)
                                addToBackStack("name")
                                commit()
                            }
                        }
                        binding.share.setOnClickListener {
                            val sendIntent: Intent = Intent().apply {
                                action = Intent.ACTION_SEND
                                putExtra(Intent.EXTRA_TEXT, ("$name ha conseguido $points puntos ! Atrévete a superarlo !").uppercase())
                                type = "text/plain"
                            }

                            val shareIntent = Intent.createChooser(sendIntent, null)
                            startActivity(shareIntent)
                        }
                    })
            })

    }
    fun fileExists(file:String): Boolean{
        val files = requireActivity().fileList()
        val exists = files.find { it == file }
        if(exists!=null) return true
        return false
    }
    fun readFile() {
        var content = ""
        val file = InputStreamReader(requireActivity().openFileInput("Score.txt"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            content += line + "\n"
            line = br.readLine()
        }
        println(content)
    }
    fun writeFile(name: String, points: Int){
        val file = OutputStreamWriter(requireActivity().openFileOutput("Score.txt", Activity.MODE_APPEND))
        file.appendLine("$name $points")
        file.flush()
        file.close()
    }
}