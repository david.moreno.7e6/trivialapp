package com.example.trivialapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.trivialapp.databinding.ActivityMenuBinding
import com.example.trivialapp.databinding.FragmentAnswersBinding
import com.example.trivialapp.databinding.FragmentMenuBinding


class MenuScreen : AppCompatActivity() {

    lateinit var binding: ActivityMenuBinding
    val randomNames= arrayOf("Pepe", "Juan", "Sara", "Sandra", "David", "Laia", "Roberto", "Carla", "Leo", "Natalia")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null){
            supportActionBar?.hide()
        }
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentMenu, MenuFragment())  //fragmentContainerView es el id en el layout, Fragment1 es el nom del fragment
            setReorderingAllowed(true) // serveixen per tornar al fragment anterior
            addToBackStack("name") // name can be null
            commit()
        }
    }
}