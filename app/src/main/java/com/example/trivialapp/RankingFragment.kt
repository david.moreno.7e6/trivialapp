package com.example.trivialapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.trivialapp.databinding.FragmentRankingBinding
import java.io.BufferedReader
import java.io.InputStreamReader

class RankingFragment : Fragment() {
    lateinit var binding: FragmentRankingBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentRankingBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        readFile()

        binding.menu.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu, MenuFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }
    fun readFile(){
        var player = mutableListOf<MutableList<String>>()
        var lines=0
        val file = InputStreamReader(requireActivity().openFileInput("Score.txt"))
        val br = BufferedReader(file)
        var line = br.readLine()
        while(line!=null){
            lines++
            player.add(line.split(" ").toMutableList())
            line = br.readLine()
        }
        player.sortByDescending { it[1]}
        var user= ""
        var result=""
        var pos=""
        var num=0
        for (game in player){
            num++
            user += "${game[0]}\n"
            result += "${game[1]}\n"
            pos += "$num\n"
        }
        binding.name.text= user
        binding.result.text= result
        binding.pos.text= pos
    }


}