package com.example.trivialapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import com.example.trivialapp.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding
    val randomNames= arrayOf("Pepe", "Juan", "Sara", "Sandra", "David", "Laia", "Roberto", "Carla", "Leo", "Natalia")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.jugarButton.setOnClickListener {

            val nom = binding.username.text.toString()
            val nomRandom= randomNames.random()
            if (nom == ""){
                val Toast= Toast.makeText(getActivity(),"No tienes nombre ? Prueba con $nomRandom", Toast.LENGTH_SHORT)
                Toast.show()
            }
            else{
                parentFragmentManager.setFragmentResult(
                    "jugador",
                    bundleOf("jugador" to nom)
                )
                parentFragmentManager.beginTransaction().apply {
                    replace(R.id.fragmentMenu, ChooseAnswers())
                    setReorderingAllowed(true)
                    addToBackStack("name")
                    commit()
                }
            }
        }
        binding.ranking.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu, RankingFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }
}