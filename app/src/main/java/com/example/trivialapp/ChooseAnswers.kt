package com.example.trivialapp

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentResultListener
import com.example.trivialapp.databinding.FragmentChooseAnswersBinding


class ChooseAnswers : Fragment() {

    lateinit var binding: FragmentChooseAnswersBinding
    lateinit var background: ConstraintLayout
    var tematica = arrayListOf<String>("HISTORIA", "DEPORTE", "CIENCIA", "CINE", "MÚSICA", "GEOGRAFÍA", "ARTE", "ADIVINANZA", "LENGUA")
    var count=0
    lateinit var name:String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentChooseAnswersBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parentFragmentManager.setFragmentResultListener(
            "dades", this
        ) { requestKey: String, result: Bundle ->
            val points = result.getInt("points")
            val list = result.getStringArrayList("list")
            if (list != null) {
                tematica = list
            }
            count= points
        }
        val timer = object : CountDownTimer(2000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

            }

            override fun onFinish() {
                    var text = binding.tematicaNom.text
                    val choosed = tematica.random()
                    background = binding.activityChooserViewContent
                    tematica.remove(choosed)
                    binding.tematicaNom.text = choosed
                    binding.tematicaNom.visibility = View.VISIBLE
                    when (choosed) {
                        "HISTORIA" -> {
                            background.setBackgroundColor(Color.parseColor("#FFA500"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.batalla.visibility = View.VISIBLE
                            binding.coliseo.visibility = View.VISIBLE
                            binding.toPlay.text = "A LUCHAR"
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }

                            }
                        }

                        "DEPORTE" -> {
                            background.setBackgroundColor(Color.parseColor("#FF8C00"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.imageBetween.setImageResource(R.drawable.ali)
                            binding.aficion.visibility = View.VISIBLE
                            binding.toPlay.text = "LISTOS? YA!"
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                        "CIENCIA" -> {
                            background.setBackgroundColor(Color.parseColor("#9ACD32"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.imageBetween.setImageResource(R.drawable.ciencia)
                            binding.portal.visibility = View.VISIBLE
                            binding.toPlay.text = "AL LABORATORIO"
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                        "CINE" -> {
                            background.setBackgroundColor(Color.parseColor("#696969"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.cine.visibility = View.VISIBLE
                            binding.palomitas.visibility = View.VISIBLE
                            binding.aficion.setImageResource(R.drawable.butacas)
                            binding.aficion.visibility = View.VISIBLE
                            binding.toPlay.text = "VER PELICULA"
                            binding.toPlay.visibility = View.VISIBLE
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                        "MÚSICA" -> {
                            background.setBackgroundColor(Color.parseColor("#7FFFD4"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.imageBetween.setImageResource(R.drawable.jackson)
                            binding.aficion.setImageResource(R.drawable.publico)
                            binding.aficion.visibility = View.VISIBLE
                            binding.toPlay.text = "A BAILAR"
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                        "GEOGRAFÍA" -> {
                            background.setBackgroundColor(Color.parseColor("#FFDAB9"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.imageBetween.setImageResource(R.drawable.mundo)
                            binding.imageBottom.setImageResource(R.drawable.mapa)
                            binding.toPlay.text = "GOOGLE MAPS"
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                        "ARTE" -> {
                            background.setBackgroundColor(Color.parseColor("#ffffff"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.imageBetween.setImageResource(R.drawable.pincel)
                            binding.manos.visibility = View.VISIBLE
                            binding.toPlay.text = "ESTOY INSPIRADO"
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                        "ADIVINANZA" -> {
                            background.setBackgroundColor(Color.parseColor("#AFEEEE"))
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.imageBetween.setImageResource(R.drawable.interrogante)
                            binding.puzzle.visibility = View.VISIBLE
                            binding.toPlay.text = "ADIVINA ADIVINANZA..."
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                        "LENGUA" -> {
                            background.setBackgroundColor(Color.parseColor("#B22222"))
                            binding.imageBetween.setImageResource(R.drawable.lengua)
                            binding.nuevaTematica.visibility = View.INVISIBLE
                            binding.toPlay.visibility = View.VISIBLE
                            binding.puzzle.setImageResource(R.drawable.letras)
                            binding.puzzle.visibility = View.VISIBLE
                            binding.toPlay.text = "PROSPEREMOS"
                            binding.toPlay.setOnClickListener {
                                val bundle = Bundle()
                                bundle.putString("tematica", choosed)
                                bundle.putStringArrayList("list", tematica)
                                bundle.putInt("result", count)
                                parentFragmentManager.setFragmentResult(
                                    "datos",
                                    bundle
                                )
                                parentFragmentManager.beginTransaction().apply {
                                    replace(R.id.fragmentMenu, FragmentAnswers())
                                    setReorderingAllowed(true)
                                    addToBackStack("name")
                                    commit()
                                }
                            }
                        }
                    }
                }
            }.start()
        }
    }
